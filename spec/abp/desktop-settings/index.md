# Options Page
Options page for the ABP Browser extension.

The options page is used to change various options of the ABP browser extension.

1. [Navigation menu](#navigation-menu)
1. [About ](#about)
1. [General tab](general-tab.md#general-tab)
1. [Allowlist tab](allowlist-tab.md#allowlist-tab)
1. [Advanced tab](advanced-tab.md#advanced-tab)
1. [Help tab](help-tab.md#help-tab)
1. [Assets](#assets)

## General requirements
### Bidirectionailty
Any change of state will be immediately reflected in the options page regardless
of where and how the change was made, so that the options page always reflects
the current state of the extension.

- If the user opens the option page more than once and makes changes to either of them, the change will be reflected in any of them and not just the one the change was made. 
- If a change to the state was made through other means e.g. auto update of filter list subscriptions, it will be reflected immediately in every instance of the options page.

### Style Guide
All UI patterns are defined in the [Options page Style Guide](/spec/abp/settings-style-guide.md).

### WCAG
Should implement the following [WCAG 2.0 Guidelines](http://www.w3.org/TR/2008/REC-WCAG20-20081211/):

- 2.1 Keyboard Accessible
- 2.4 Navigable

### Mouse behaviour
Anywhere an item is clickable the mouse cursor should follow the default browser behaviour.

## Navigation
Implement <https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/#tabpanel>

## Navigation menu
### Navigation menu default
![](/res/abp/desktop-settings/logo_Settings.png)

1. [Main title](#main-title)
1. [Navigation menu list](#navigation-menu-list)
1. [Contribute button](#contribute-button)
1. [About link](#about-link)

#### Main title
Logo [image](/res/abp/logo/ABP-logo-full.svg)

`Settings`

This will appear consistently across all tabs.

All elements within the navigation menu are fixed according to the screen height.

#### Navigation menu list
Navigation names should match the headlines of each tab.
1. [General tab headline](#general-tab-headline)
1. [Allowlisted tab headline](#Allowlisted-tab-headline)
1. [Advanced tab headline](#advanced-tab-headline)
1. [Help tab headline](#help-tab-headline)

#### Contribute button
Opens the [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=contribute` in a new tab

#### About link
Opens the [About](#about) overlay.

### About
![](/res/abp/desktop-settings/navigation-about.jpg)

#### Title
`About Adblock Plus`

#### Version number 
Version number of extension added dynamically. 

`Version number xxx`

#### Text
`Copyright © {current year} [eyeo GmbH]`[1]

`All rights reserved. Adblock Plus® is a registered  trademark of eyeo GmbH.`

`Privacy Policy`[2]

[1] eyeo GmbH - [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=imprint`

[2] [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=privacy`

#### Button to close layover
`CLOSE` 

X closes the layover.

### Store Rating

This feature should be enabled for ABP extension (both stable  and development) versions for the following browsers: Chrome, Firefox and Opera only

Design: [Invision Link](https://eyeogmbh.invisionapp.com/share/J9X0PA2PD3F#/screens)

Waving icon: [Link](res/abp/desktop-settings/assets/waving.svg)
Star icon: [Link](res/abp/desktop-settings/assets/rating_star.svg)
Heart icon: [Link](res/abp/desktop-settings/assets/heart.svg)

On the right area should be a heart icon, clicking on it should open a dialog with the message that asks the user to rank us in the stores, and `Rate` and `Donate` buttons.
- Click on `Rate us` opens the relevant store, directly to the review page if possible.
  - CWS URL should include parameter "ref=store-rating" so we can see it in Google Analytics
- Click on `Donate` opens donation page [Documentation link](/spec/abp/prefs.md#documentation-link): `%LINK%=donate_settings_page`
- Keep showing this feature, even if user clicked on the button.

| Dialog text | Content |
|----------------|----------------|
| Title | `Enjoying Adblock Plus?` |
| Description | `Please take a moment to rate ABP or donate to the project. Thanks for your support!` |

| Store | Link |
|-------|------|
|Chrome|`%LINK%=chrome_review`|
|Firefox|`%LINK%=firefox_review`|
|Opera|`%LINK%=opera_review`|

## Premium header

### Free users

An upgrade header is shown to all ABP users who are either on the free plan (because they never upgraded they have an expired subscription), or that are on a premium plan but have not confirmed their account on that particular instance of the extension (due to it being installed on a different browser, or a different browser profile where applicable).

**Design:** https://www.figma.com/file/mU4E9k2cSoKgUBEeke51yU/ABP-Premium?node-id=181%3A2045

#### Upgrade header demo

![](/res/abp/desktop-settings/upgrade-settings-demo.png) 

#### Upgrade message content

* **Pre-heading:** 
  * Crown icon, see [assets](#assetts)
  * Text: `Premium`
* **Upgrade message:**
  * Text: `Customize and enhance your ad blocking experience! Learn more`
* **Upgrade CTA:**
  * Button label: `Upgrade`
  
Behind the `Learn more` link in the _upgrade header_ and the _upgrade CTA_ there's a link pointing to `https://accounts.adblockplus.org/%LANG%/premium`. In the link, the `%LANG%` placeholder will be replaced by the locale code (e.g. `en_US`) and, if possible, should contain all of the following query parameters:

| Parameter | Description |
|-|-|
|`%ADDON_NAME%`|Extension name|
|`%ADDON_VERSION%`|Extension version|
|`%APPLICATION_NAME%`|Browser name|
|`%APPLICATION_VERSION%`|Browser version|
|`%PLATFORM_NAME%`|Browser engine name|
|`%PLATFORM_VERSION%`|Browser engine version|
|`%SOURCE%`|Equals `desktop-settings`|

### Premium users

Users who have an active premium plan will see a header that confirms their subscription type and a link that sends them to the account portal, from where they can manage their subscription.

**Design:** https://www.figma.com/file/mU4E9k2cSoKgUBEeke51yU/ABP-Premium?node-id=433%3A1941

#### Active premium plan header demo

![](/res/abp/desktop-settings/manage-account-settings-demo.png) 

#### Active premium plan header content

* **Manage account link:**
  * Link label: `Manage my subscription`
  * Link href: `https://accounts.adblockplus.org/%LANG%/manage`
* **Premium button label:**
  * Crown icon, see [assets](#assets)
  * Button label text: `Premium`
  * Link behind button: `https://accounts.adblockplus.org/%LANG%/manage`

In the links, the `%LANG%` placeholder will be replaced by the locale code (e.g. `en_US`) and should contain all of the following query parameters:

| Parameter | Description |
|-|-|
|`%LICENSE_CODE%`|Equals the Premium License code received from the license server|
|`%SOURCE%`|Equals `desktop-settings`|

## Assets
[Back to top of page](#options-page)

| Name | Asset | 
|-----------|---------------|
| ABP-icon-outline.svg | [<img src="/res/abp/logo/ABP-icon-outline.svg" width="50"/>](/res/abp/logo/ABP-icon-outline.svg) |
| attention.svg | ![](/res/abp/desktop-settings/assets/attention.svg) |
| box-circle.svg | ![](/res/abp/desktop-settings/assets/box-circle.svg) |
| checkbox-empty.svg | ![](/res/abp/desktop-settings/assets/checkbox-empty.svg) |
| checkbox-yes.svg | ![](/res/abp/desktop-settings/assets/checkbox-yes.svg) |
| checkmark.svg | ![](/res/abp/desktop-settings/assets/checkmark.svg) |
| code.svg | ![](/res/abp/desktop-settings/assets/code.svg) |
| copy-content.png | ![](/res/abp/desktop-settings/assets/copy-content.png) ![](/res/abp/desktop-settings/assets/copy-content@2x.png) |
| delete.svg | ![](/res/abp/desktop-settings/assets/delete.svg) |∂
| email.svg | ![](/res/abp/desktop-settings/assets/email.svg) |
| facebook.svg | ![](/res/abp/desktop-settings/assets/facebook.svg) |
| globe.svg | ![](/res/abp/desktop-settings/assets/globe.svg) |
| googleplus.svg | ![](/res/abp/desktop-settings/assets/googleplus.svg) |
| reload.svg | ![](/res/abp/desktop-settings/assets/reload.svg) |
| settings.svg | ![](/res/abp/desktop-settings/assets/settings.svg) |
| snail.svg | ![](/res/abp/desktop-settings/assets/snail.svg) |
| tooltip.svg | ![](/res/abp/desktop-settings/assets/tooltip.svg) |
| tooltip-arrow.svg | ![](/res/abp/desktop-settings/assets/tooltip-arrow.svg) |
| trash.svg | ![](/res/abp/desktop-settings/assets/trash.svg) |
| twitter.svg | ![](/res/abp/desktop-settings/assets/twitter.svg) |
| toggle-enabled.svg | ![](/res/abp/desktop-settings/assets/toggle-enabled.svg) |
| toggle-disabled.svg | ![](/res/abp/desktop-settings/assets/toggle-disabled.svg) |
| weibo.svg | ![](/res/abp/desktop-settings/assets/weibo.svg) | 
| premium-crown.svg | ![](/res/abp/desktop-settings/assets/premium-crown.svg) | 
